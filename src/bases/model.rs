use serde::{Serialize, Deserialize};
use actix_web::{HttpResponse, HttpRequest, Responder, Error};
use futures::future::{ready, Ready};
use sqlx::{PgPool, FromRow, Row};
use sqlx::postgres::PgRow;
use anyhow::Result;

#[derive(Serialize, Deserialize)]
pub struct BaseRequest {
    name: String,
    value: String
}

#[derive(Serialize, FromRow)]
pub struct Base {
    id: i32,
    name: String,
    value: String
}

impl Base {
    pub async fn find_by_id(id: i32, pool: &PgPool) -> Result<Vec<Base>> {
        let mut bases = vec![];
        let rows = sqlx::query!(
            r#"
                SELECT b.id, name, value FROM bases b, users u 
                WHERE b.user_id = u.id and u.id = $1 ORDER BY b.id
            "#,
            id
        )
        .fetch_all(pool)
        .await?;

        for row in rows {
            bases.push(Base {
                id: row.id,
                name: row.name,
                value: row.value
            });
        }
        Ok(bases)
    }

    // pub async fn find_by_userid(id: i32, pool: &PgPool) -> Result<Vec<Base>> {
    //     let mut posts = vec![];
    //     let rows = sqlx::query!(
    //         r#"
    //             SELECT b.id, b.value, description FROM bases b, users u 
    //             WHERE b.user_id = u.id and u.id = $1 ORDER BY b.id
    //         "#,
    //         id
    //     )
    //     .fetch_all(pool)
    //     .await?;

    //     for row in rows {
    //         posts.push(Post {
    //             id: row.id,
    //             title: row.title,
    //             description: row.description
    //         });
    //     }
    //     Ok(posts)
    // }

    pub async fn create(user_id: i32, base: BaseRequest, pool: &PgPool) -> Result<Base> {
        let mut tx = pool.begin().await?;
        let base = sqlx::query("INSERT INTO bases (user_id, name, value) VALUES ($1, $2, $3) RETURNING id, name, value")
            .bind(user_id)
            .bind(&base.name)
            .bind(base.value)
            .map(|row: PgRow| {
                Base {
                    id: row.get(0),
                    name: row.get(1),
                    value: row.get(2)
                }
            })
            .fetch_one(&mut tx)
            .await?;

        tx.commit().await?;
        Ok(base)
    }
}