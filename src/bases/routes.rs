use crate::bases::{Base, BaseRequest};
use actix_web::{delete, get, post, put, web, HttpResponse, Responder};
use sqlx::PgPool;

#[get("/bases/{id}")]
async fn find(id: web::Path<i32>, db_pool: web::Data<PgPool>) -> impl Responder {
    let result = Base::find_by_id(id.into_inner(), db_pool.get_ref()).await;
    match result {
        Ok(bases) => HttpResponse::Ok().json(bases),
        _ => HttpResponse::BadRequest().body("Bases for user not found")
    }
}

#[post("/bases/{id}")]
async fn create(id: web::Path<i32>, base: web::Json<BaseRequest>, db_pool: web::Data<PgPool>) -> impl Responder {
    let result = Base::create(id.into_inner(), base.into_inner(), db_pool.get_ref()).await;
    match result {
        Ok(base) => HttpResponse::Ok().json(base),
        _ => HttpResponse::BadRequest().body("Error trying to create new todo")
    }
}

// Configure routes for this module
pub fn init(cfg: &mut web::ServiceConfig) {
    // cfg.service(find_all);
     cfg.service(find);
    cfg.service(create);
    // cfg.service(update);
    // cfg.service(delete);
}