use serde::{Serialize, Deserialize};
use actix_web::{HttpResponse, HttpRequest, Responder, Error};
use futures::future::{ready, Ready};
use sqlx::{PgPool, FromRow, Row};
use sqlx::postgres::PgRow;
use anyhow::Result;

#[derive(Serialize, Deserialize)]
pub struct UserRequest {
    email: String,
    password: String
}

#[derive(Serialize, FromRow)]
pub struct User {
    id: i32,
    email: String
}

// impl Responder for Users {
//     type Error = Error;
//     type Future = Ready<Result<HttpResponse, Error>>;

//     fn respond_to(self, _req: &HttpRequest) -> Self::Future {
//         let body = serde_json::to_string(&self).unwrap();
//         // create response and set content type
//         ready(Ok(
//             HttpResponse::Ok()
//                 .content_type("application/json")
//                 .body(body)
//         ))
//     }
// }

impl User {
    pub async fn find_all(pool: &PgPool) -> Result<Vec<User>> {
        let mut users = vec![];
        let rows = sqlx::query!(
            r#"
                SELECT id, email, password FROM users ORDER BY id
            "#
        )
        .fetch_all(pool)
        .await?;

        for row in rows {
            users.push(User {
                id: row.id,
                email: row.email
            });
        }
        Ok(users)
    }

    pub async fn create(user: UserRequest, pool: &PgPool) -> Result<User> {
        let mut tx = pool.begin().await?;
        let user: User = sqlx::query("INSERT INTO users (email,password) VALUES($1, $2) RETURNING id, email")
            .bind(&user.email)
            .bind(&user.password)
            .map(|row: PgRow| {
                User {
                    id: row.get(0),
                    email: row.get(1)
                }
            })
            .fetch_one(&mut tx)
            .await?;

        tx.commit().await?;
        Ok(user)
    }
}