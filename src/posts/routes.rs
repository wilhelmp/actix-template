use crate::posts::{Post, PostRequest};
use actix_web::{delete, get, post, put, web, HttpResponse, Responder};
use sqlx::PgPool;

#[get("/posts/{id}")]
async fn find(id: web::Path<i32>, db_pool: web::Data<PgPool>) -> impl Responder {
    let result = Post::find_by_id(id.into_inner(), db_pool.get_ref()).await;
    match result {
        Ok(posts) => HttpResponse::Ok().json(posts),
        _ => HttpResponse::BadRequest().body("Posts for user not found")
    }
}

#[post("/posts/{id}")]
async fn create(id: web::Path<i32>, post: web::Json<PostRequest>, db_pool: web::Data<PgPool>) -> impl Responder {
    let result = Post::create(id.into_inner(), post.into_inner(), db_pool.get_ref()).await;
    match result {
        Ok(post) => HttpResponse::Ok().json(post),
        _ => HttpResponse::BadRequest().body("Error trying to create new todo")
    }
}

// Configure routes for this module
pub fn init(cfg: &mut web::ServiceConfig) {
    // cfg.service(find_all);
     cfg.service(find);
    cfg.service(create);
    // cfg.service(update);
    // cfg.service(delete);
}