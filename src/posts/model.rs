use serde::{Serialize, Deserialize};
use actix_web::{HttpResponse, HttpRequest, Responder, Error};
use futures::future::{ready, Ready};
use sqlx::{PgPool, FromRow, Row};
use sqlx::postgres::PgRow;
use anyhow::Result;

#[derive(Serialize, Deserialize)]
pub struct PostRequest {
    title: String,
    description: String
}

#[derive(Serialize, FromRow)]
pub struct Post {
    id: i32,
    title: String,
    description: String
}

impl Post {
    pub async fn find_by_id(id: i32, pool: &PgPool) -> Result<Vec<Post>> {
        let mut posts = vec![];
        let rows = sqlx::query!(
            r#"
                SELECT p.id, title, description FROM posts p, users u 
                WHERE p.user_id = u.id and u.id = $1 ORDER BY p.id
            "#,
            id
        )
        .fetch_all(pool)
        .await?;

        for row in rows {
            posts.push(Post {
                id: row.id,
                title: row.title,
                description: row.description
            });
        }
        Ok(posts)
    }

    pub async fn create(user_id: i32, post: PostRequest, pool: &PgPool) -> Result<Post> {
        let mut tx = pool.begin().await?;
        let post = sqlx::query("INSERT INTO posts (user_id, title, description) VALUES ($1, $2, $3) RETURNING id, title, description")
            .bind(user_id)
            .bind(&post.title)
            .bind(post.description)
            .map(|row: PgRow| {
                Post {
                    id: row.get(0),
                    title: row.get(1),
                    description: row.get(2)
                }
            })
            .fetch_one(&mut tx)
            .await?;

        tx.commit().await?;
        Ok(post)
    }
}